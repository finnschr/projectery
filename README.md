# Projectery

Platform for organizations to team up and to match projects and contributors

## Features
* Pitch your project with text and media to make it visible to all members
* Emphasize where you need help in your project and which skills are needed
* Get suggestions of members who's skills might match the needed skills in your project
* Browse through projects of other members and easily find the one you are interested in
* Get suggestions of projects where your skills would be highly appreciated
* Find contact details to get in touch with project starters/potential contributors quickly and start collaboration
* Find projects that share similiar goals to possibly unite or exchange knowledge
